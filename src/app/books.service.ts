import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

  books:any = [{title:'Alice in Wonderland', author:'Lewis Carrol'},{title:'War and Peace', author:'Leo Tolstoy'}, {title:'The Magic Mountain', author:'Thomas Mann'}, {title:'A Song Of Ice And Fire', author:'George R R Martin'}]  

  constructor() { }

  /*
  getBooks(){
    const booksObservable = new Observable(
      observer => {
        setInterval(
          () => observer.next(this.books),2000
        )
      }
    )
    return booksObservable;
   }
  */

  getBooks(){
    return this.books;
  }

}
