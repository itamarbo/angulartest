import { PostsService } from './../posts.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-myposts',
  templateUrl: './myposts.component.html',
  styleUrls: ['./myposts.component.css']
})
export class MypostsComponent implements OnInit {

  constructor(public postsService:PostsService) { }

  posts:any=[];

  deletePost(id:string){
    this.postsService.deletePost(id);
  }

  ngOnInit() {
    this.postsService.getMyPosts().subscribe(posts =>{
      console.log(posts);
      this.posts=posts;
    })
  }

}
