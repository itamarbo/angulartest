import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  myurl="https://jsonplaceholder.typicode.com/posts";
  myurlcCom="https://jsonplaceholder.typicode.com/posts/1/comments"

  userCollection: AngularFirestoreCollection = this.db.collection('users');
  postCollection: AngularFirestoreCollection;

  constructor(public httpClient:HttpClient, public db:AngularFirestore) { }
  
  getPosts(){
    return this.httpClient.get(this.myurl);
  }

  getComments(){
    return this.httpClient.get(this.myurlcCom);
  }

  getMyPosts():Observable<any[]>{
    return this.db.collection('posts').valueChanges({idField:'id'});
  }

  addPost(title:string, body:string){
    const post = {title:title, body:body};
    this.db.collection('posts').add(post);
  }

  deletePost(id:string){
    this.db.doc(`posts/${id}`).delete();
  }

  addPosts(title:string, body:string){
    const post = {title:title, body:body,};
    this.db.collection('posts').add(post);
  }
}
