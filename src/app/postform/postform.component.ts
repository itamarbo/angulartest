import { Router } from '@angular/router';
import { PostsService } from './../posts.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-postform',
  templateUrl: './postform.component.html',
  styleUrls: ['./postform.component.css']
})
export class PostformComponent implements OnInit {

  title:string;
  body:string;

  constructor(public postsService:PostsService, public router:Router) { }

  ngOnInit() {
  }

  onSubmit(){
    this.postsService.addPost(this.title, this.body)
    this.router.navigate(['/myposts']);
  }  
}
