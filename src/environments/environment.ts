// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyB2KiPsDE59_4MFivb7cycmc7ixlbu-TYI",
    authDomain: "angulartest-de5ef.firebaseapp.com",
    databaseURL: "https://angulartest-de5ef.firebaseio.com",
    projectId: "angulartest-de5ef",
    storageBucket: "angulartest-de5ef.appspot.com",
    messagingSenderId: "999725247481",
    appId: "1:999725247481:web:3e85c1388e32b9d68ceb28",
    measurementId: "G-63J70QLQD2"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
